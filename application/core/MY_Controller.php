<?php

/**
* Global Controller
*/
class MY_Controller extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}


	public function master_layout($view)
	{
		$this->load->view('/master/header');
		$this->load->view($view);
		$this->load->view('/master/footer');
	}
}